#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# API: Creating CLI.
# ==============================================================================
from argparse import ArgumentParser


def add_config_option(parser, required=False):
    parser.add_argument(
        '-c',
        '--config',
        help=r'Please pass the path of the configuration file like "C:\Notos\Software\visara\config.ini"',
        default="",
        required=required
    )
    return parser


def add_verbose_debug(parser):
    parser.add_argument(
        '-v',
        '--verbose',
        help=r'Enable verbose output.',
        action="store_true",
        default=False
    )
    parser.add_argument(
        '-d',
        '--debug',
        help=r'Enable debug output.',
        action="store_true",
        default=False
    )
    return parser


def arg_parser():
    parser = ArgumentParser(
        prog="visara",
        description="Set up visara, synchronize with ftp server, plot, export data and more."
    )
    parser.set_defaults(
        which='main'
    )
    subparsers = parser.add_subparsers()
    # ---

    config_parser = subparsers.add_parser(
        'config',
        help="Save the location of a default / or generate a config file."
    )
    config_parser.add_argument(
        '-s',
        '--set',
        help=r'Set the path of a configuration file like "C:\Notos\Software\visara\config.ini"',
        default=""
    )
    config_parser.add_argument(
        '-g',
        '--generate',
        help=r'Provide a path where a configuration file should be generated '
             r'like "C:\Notos\Software\visara\config.ini"',
        default=""
    )
    config_parser = add_verbose_debug(
        config_parser
    )
    config_parser.set_defaults(
        which='config'
    )
    # ---

    control_parser = subparsers.add_parser(
        'control',
        help="Basic controls for initiation and resetting the database."
    )
    control_parser.set_defaults(
        which='control'
    )
    control_parser = add_config_option(
        control_parser
    )
    control_parser = add_verbose_debug(
        control_parser
    )
    control_parser.add_argument(
        '--init',
        help=r'initiate folders, database and table. File from ftp server must be passed for initiation.',
        default=""
    )
    control_parser.add_argument(
        '--reset',
        help=r'DELETES DATABASE AND CONFIG FILE LOCATION!',
        action="store_true",
        default=False
    )
    control_parser.add_argument(
        '--force',
        help=r'This flag must be used to perform a reset!!',
        action="store_true",
        default=False
    )
    # ---

    synchronize_parser = subparsers.add_parser(
        'synchronize',
        help="Synchronize ftp server with local files and database."
    )
    synchronize_parser = add_config_option(
        synchronize_parser
    )
    synchronize_parser = add_verbose_debug(
        synchronize_parser
    )
    synchronize_parser.set_defaults(
        which='synchronize'
    )
    synchronize_parser.add_argument(
        '--now',
        help=r'Execute synchronization of ftp server with local files and database.',
        action="store_true",
        default=False
    )
    # ---

    plot_parser = subparsers.add_parser(
        'plot',
        help="Visualize data from database."
    )
    plot_parser = add_config_option(
        plot_parser
    )
    plot_parser = add_verbose_debug(
        plot_parser
    )
    plot_parser.set_defaults(
        which='plot'
    )
    plot_parser.add_argument(
        '-s',
        '--section',
        help=r'Provide the section of the configuration file that you want to use for plotting like "PLOT_7D"',
        default="PLOT_DEFAULT"
    )
    plot_parser.add_argument(
        '--now',
        help=r'Execute the plotting of data as specified in the configuration file.',
        action="store_true",
        default=False
    )
    # ---

    export_parser = subparsers.add_parser(
        'export',
        help="Save data from database to csv file."
    )
    export_parser = add_config_option(
        export_parser
    )
    export_parser = add_verbose_debug(
        export_parser
    )
    export_parser.set_defaults(
        which='export'
    )
    export_parser.add_argument(
        '-s',
        '--section',
        help=r'Provide the section of the configuration file that you want to use for exporting like "EXPORT_ALL"',
        default="EXPORT_DEFAULT"
    )
    export_parser.add_argument(
        '--mapped',
        help=r'Apply mapping as specified in the CHANNEL_TO_VALUE section before saving data.',
        action="store_true",
        default=False
    )
    export_parser.add_argument(
        '--now',
        help=r'Execute the data export as specified in the configuration file.',
        action="store_true",
        default=False
    )
    # ---

    return parser
