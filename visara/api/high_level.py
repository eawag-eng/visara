#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# API: Top level user api.
# ==============================================================================
from visara.core.data_functionality import (
    generate_table_creation_query,
    init_folders_and_database,
    delete_database,
    synchronize_with_ftp,
    update_database,
    read_database,
    save_data,
    parse_file
)
from visara.core.plotting import (
    plot_data,
    get_group_column,
    getting_mapping_values,
    map_channels_to_values
)
from visara.core.datetime_parser import parse_datetime
from visara.core.memory import delete_ini_file_location
from visara import (
    LOGGER,
    log_error
)

from os.path import join


def init(config, file):
    """
    Initiate database, table and create folders.

    Parameters
    ----------
    config : dict
        read configuration file.
    file : str
        you must pass a file from the stp server so that it can be read and the database generated

    Notes
    -----
    Necessary config sections and keys are

    DATABASE: create_table
    DATABASE: table_name
    DATABASE: database_file
    FILE_SETTINGS: delimiter
    FILE_SETTINGS: date_col_name
    FILE_SETTINGS: time_col_name
    FILE_SETTINGS: datetime_col_name
    FILE_SETTINGS: date_time_format
    PATHS: data_directory
    PATHS: plot_directory
    PATHS: export_directory
    PATHS: logging_directory
    NOTIFICATIONS: email
    NOTIFICATIONS: log_file

    Example
    -------

    config = {
        'DATABASE': {
            'table_name': 'visara',
            ...
        },
        'FILE_SETTINGS': {
            'delimiter': ',',
            ...
        },
        'PATHS': {
            'data_directory': 'C:/visara/data',
            ...
        },
        'NOTIFICATIONS': {
            'log_file': 'visara.log',
            ...
        }
    }

    """
    LOGGER.info("Initiating VisARA!")

    try:

        data = parse_file(
            file,
            sep=config["FILE_SETTINGS"]["delimiter"],
            date_col=config["FILE_SETTINGS"]["date_col_name"],
            time_col=config["FILE_SETTINGS"]["time_col_name"],
            datetime_col=config["FILE_SETTINGS"]["datetime_col_name"],
            date_time_format=config["FILE_SETTINGS"]["date_time_format"],
        )
        query_string = generate_table_creation_query(
            data,
            table_name=config["DATABASE"]["table_name"]
        )

        init_folders_and_database(
            folders=[
                config["PATHS"]["data_directory"],
                config["PATHS"]["plot_directory"],
                config["PATHS"]["export_directory"],
                config["PATHS"]["logging_directory"]
            ],
            database=join(
                config["PATHS"]["data_directory"],
                config["DATABASE"]["database_file"]
            ),
            query=query_string
        )

    except Exception as err:
        log_error(
            config,
            err
        )
        raise err


def reset(config):
    """
    Resets/Deletes database, table and default config location.

    Parameters
    ----------
    config : dict
        read configuration file.

    Notes
    -----
    Necessary config sections and keys are

    DATABASE: database_file
    PATHS: data_directory
    PATHS: logging_directory
    NOTIFICATIONS: email
    NOTIFICATIONS: log_file

    Example
    -------

    config = {
        'DATABASE': {
            'database_file': 'database.sqlite',
            ...
        },
        'PATHS': {
            'data_directory': 'C:/visara/data',
            ...
        },
        'NOTIFICATIONS': {
            'log_file': 'visara.log',
            ...
        }
    }

    """
    LOGGER.info("Resetting the Database and INI file location!")

    try:
        delete_ini_file_location()
        delete_database(
            join(
                config["PATHS"]["data_directory"],
                config["DATABASE"]["database_file"]
            )
        )

    except Exception as err:
        log_error(
            config,
            err
        )
        raise err


def synchronize(config):
    """
    Synchronizes ftp server with local storage and database.

    Parameters
    ----------
    config : dict
        read configuration file.

    Notes
    -----
    Necessary config sections and keys are

    DATABASE: table_name
    DATABASE: database_file
    FILE_SETTINGS: file_name_pattern
    FILE_SETTINGS: delimiter
    FILE_SETTINGS: date_col_name
    FILE_SETTINGS: time_col_name
    FILE_SETTINGS: datetime_col_name
    FILE_SETTINGS: date_time_format
    PATHS: data_directory
    PATHS: logging_directory
    NOTIFICATIONS: email
    NOTIFICATIONS: log_file
    FTP: host
    FTP: usr
    FTP: pwd
    FTP: path

    Example
    -------

    config = {
        'DATABASE': {
            'table_name': 'visara',
            ...
        },
        'PATHS': {
            'data_directory': 'C:/visara/data',
        },
        'FTP': {
            'host': '192.168.1.32',
            'usr': 'wago',
            'pwd': 'axsdf830'
            'path': '/'
        },
        'NOTIFICATIONS': {
            'log_file': 'visara.log',
            ...
        }
    }

    """
    LOGGER.info("Synchronizing ftp server with local files and database!")

    try:
        synchronize_with_ftp(
            local_folder=config["PATHS"]["data_directory"],
            remote_details=config["FTP"],

        )

        _ = update_database(
            data_folder=config["PATHS"]["data_directory"],
            database=join(
                config["PATHS"]["data_directory"],
                config["DATABASE"]["database_file"]
            ),
            return_filled=False,
            file_name_pattern=config["FILE_SETTINGS"]["file_name_pattern"],
            table_name=config["DATABASE"]["table_name"],
            sep=config["FILE_SETTINGS"]["delimiter"],
            date_col=config["FILE_SETTINGS"]["date_col_name"],
            time_col=config["FILE_SETTINGS"]["time_col_name"],
            datetime_col=config["FILE_SETTINGS"]["datetime_col_name"],
            date_time_format=config["FILE_SETTINGS"]["date_time_format"],
        )

    except Exception as err:
        log_error(
            config,
            err
        )
        raise err


def plot(config, section="PLOT_DEFAULT"):
    """
    Initiate database, table and create folders.

    Parameters
    ----------
    config : dict
        read configuration file.
    section : str
        name of section in config file that contains settings for plotting.

    Notes
    -----
    Necessary config sections and keys are

    DATABASE: table_name
    DATABASE: database_file
    PATHS: data_directory
    PATHS: plot_directory
    PATHS: logging_directory
    NOTIFICATIONS: email
    NOTIFICATIONS: log_file
    {section}: time_start
    {section}: time_end
    {section}: accumulate
    {section}: line_width
    {section}: marker_size
    {section}: colors
    {section}: columns_to_plot_pattern
    {section}: identifier
    {section}: parameter_to_unit

    Example
    -------

    config = {
        'DATABASE': {
            'table_name': 'visara',
            ...
        },
        'PATHS': {
            'data_directory': 'C:/visara/data',
            ...
        },
        'NOTIFICATIONS': {
            'log_file': 'visara.log',
            ...
        },
        section: {
            'time_start': '-5day'
        }
    }

    """
    LOGGER.info(f"Plotting data with settings from '{section}'!")

    try:
        data = read_database(
            database=join(
                config["PATHS"]["data_directory"],
                config["DATABASE"]["database_file"]
            ),
            time_start=parse_datetime(
                config[section]["time_start"]
            ),
            time_end=parse_datetime(
                config[section]["time_end"]
            ),
            table_name=config["DATABASE"]["table_name"]
        )

        mapping_values = getting_mapping_values(
            config=config,
            section_prefix="CHANNEL_TO_VALUE"
        )

        data = map_channels_to_values(
            data=data,
            mapping_dict=mapping_values
        )

        plot_data(
            data=data,
            group_by_column=get_group_column(
                config=config,
                section_prefix="CHANNEL_TO_VALUE"
            ),
            exclude_columns=config[section]["exclude_columns"],
            accumulate=config[section]["accumulate"],
            save_path=config["PATHS"]["plot_directory"],
            color_map=config[section]["colors"],
            marker_size=config[section]["marker_size"],
            line_width=config[section]["line_width"],
            identifier=config[section]["identifier"],
            parameter_unit_map=config[section]["parameter_to_unit"]
        )

    except Exception as err:
        log_error(
            config,
            err
        )
        raise err


def export(config, section="EXPORT_DEFAULT", with_mapped_channel_to_value=False):
    """
    Initiate database, table and create folders.

    Parameters
    ----------
    config : dict
        read configuration file.
    section : str
        name of section in config file that contains settings for plotting.
    with_mapped_channel_to_value : bool
        apply channel mapping to data before saving to csv.

    Notes
    -----
    Necessary config sections and keys are

    DATABASE: table_name
    DATABASE: database_file
    PATHS: data_directory
    PATHS: export_directory
    PATHS: logging_directory
    NOTIFICATIONS: email
    NOTIFICATIONS: log_file
    {section}: time_start
    {section}: time_end

    Example
    -------

    config = {
        'DATABASE': {
            'table_name': 'visara',
            ...
        },
        'PATHS': {
            'data_directory': 'C:/visara/data',
            ...
        },
        'NOTIFICATIONS': {
            'log_file': 'visara.log',
            ...
        },
        section: {
            'time_start': '-5day'
        }
    }

    """
    LOGGER.info(f"Exporting data with settings from '{section}'!")

    try:
        data = read_database(
            database=join(
                config["PATHS"]["data_directory"],
                config["DATABASE"]["database_file"]
            ),
            time_start=parse_datetime(
                config[section]["time_start"]
            ),
            time_end=parse_datetime(
                config[section]["time_end"]
            ),
            table_name=config["DATABASE"]["table_name"]
        )

        if with_mapped_channel_to_value:
            mapping_values = getting_mapping_values(
                config=config,
                section_prefix="CHANNEL_TO_VALUE"
            )

            data = map_channels_to_values(
                data=data,
                mapping_dict=mapping_values
            )

        save_data(
            data=data,
            save_path=config["PATHS"]["export_directory"]
        )

    except Exception as err:
        log_error(
            config,
            err
        )
        raise err
