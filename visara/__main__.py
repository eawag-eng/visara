#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Main: Specifying the CLI logic
# ==============================================================================
from visara.api.argparser import arg_parser
from visara.core.config import read_config
from visara.core.memory import choose_ini_file
from visara import LOGGER

from pathlib import Path


def set_output(verbose, debug):
    global LOGGER
    if verbose:
        LOGGER.verbose_on()
    if debug:
        LOGGER.debug_on()


def print_sub_parser_help(parser, subparser_name):
    parser._subparsers._group_actions[0].choices[subparser_name].print_help()


parser = arg_parser()
args = parser.parse_args()

if args.which == "config":

    if not args.set and not args.generate:
        print_sub_parser_help(parser, "config")
    else:
        set_output(
            args.verbose,
            args.debug
        )

        if args.generate:
            from visara.core.generate import generate_conf
            if not Path(args.generate).suffix:
                parser.error(r'Please provide a path with filename like "C:\config.ini"')
            else:
                generate_conf(
                    args.generate
                )

        if args.set:
            from visara.core.memory import set_ini_file_location
            if not Path(args.set).exists():
                parser.error('The file you provided does not exist: "{}"'.format(args.set))
            else:
                read_config(args.set)
                set_ini_file_location(args.set)
                print("Configuration file set!")

elif args.which == "control":

    if not args.init and not args.reset:
        print_sub_parser_help(parser, "control")
    else:
        set_output(
            args.verbose,
            args.debug
        )

    if args.reset and not args.force:
        print(
            "Are you sure you want to delete the database? You will loose all data! "
            "If your sure you must use the the --force flag with the --reset flag."
        )

    if args.reset and args.force:
        from visara.api.high_level import reset
        file = choose_ini_file(args.config)
        config = read_config(file)
        reset(config)

    if args.init:
        from visara.api.high_level import init
        if not Path(args.init).exists():
            parser.error(
                f"The file you passed '{args.init}' does not exist. "
                f"Please pass a csv file from the ftp server, that you set the config file for!"
            )
        file = choose_ini_file(args.config)
        config = read_config(file)
        init(config, args.init)

elif args.which == "synchronize":
    if not args.now:
        print_sub_parser_help(parser, "synchronize")
    else:
        set_output(
            args.verbose,
            args.debug
        )
        from visara.api.high_level import synchronize
        file = choose_ini_file(args.config)
        config = read_config(file)
        synchronize(config)

elif args.which == "plot":
    if not args.now:
        print_sub_parser_help(parser, "plot")
    else:
        set_output(
            args.verbose,
            args.debug
        )
        from visara.api.high_level import plot
        file = choose_ini_file(args.config)
        config = read_config(file)
        plot(
            config,
            section=args.section
        )

elif args.which == "export":
    if not args.now:
        print_sub_parser_help(parser, "export")
    else:
        set_output(
            args.verbose,
            args.debug
        )
        from visara.api.high_level import export

        file = choose_ini_file(args.config)
        config = read_config(file)
        export(
            config,
            section=args.section,
            with_mapped_channel_to_value=args.mapped
        )

elif args.which == "main":
    parser.print_help()
