#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Core: Ftp utilities to communicate with ftp server.
# ==============================================================================
import os
from ftplib import FTP
from io import BytesIO
import hashlib


class FtpUtil:
    """basic ftp utility to download data from the ftp server"""

    def __init__(self, host, usr, pwd, path="/", port=21):
        self.host = host
        self.usr = usr
        self.pwd = pwd
        self.rootpath = path
        self.port = port

        self.__test_conn()

    def __login(self):
        self.ftp = FTP()
        self.ftp.connect(host=self.host, port=self.port)
        self.ftp.login(user=self.usr, passwd=self.pwd)

    def __logout(self):
        self.ftp.quit()

    def __test_conn(self):
        try:
            self.__login()
            self.__logout()

        except Exception as e:
            print(f"Could not connect to the ftp server "
                  f"host: {self.host}, user: {self.usr}, pw: {self.pwd}, port: {self.port}.")
            raise e

    def ls(self, path=""):
        self.__login()
        self.ftp.cwd(f"{self.rootpath}/{path}")
        files = self.ftp.nlst()
        self.__logout()
        return files

    def get(self, sourcepath, file):
        self.__login()
        self.ftp.cwd(f"{self.rootpath}/{sourcepath}")
        bio = BytesIO()
        self.ftp.retrbinary('RETR ' + file, bio.write)
        self.__logout()
        bio.seek(0)
        return bio

    def upload(self, file, destpath="", filename=""):
        if filename == "":
            filename = os.path.split(file)[1]
        self.__login()
        if destpath != ".":
            self.ftp.cwd(f"{self.rootpath}/{destpath}")
        with open(file, "rb") as f:
            # use FTP's STOR command to upload the file
            self.ftp.storbinary(f"STOR {filename}", f)
        self.__logout()

    def download(self, sourcepath, destpath, file):

        self.__login()
        self.ftp.cwd(f"{self.rootpath}/{sourcepath}")
        with open(os.path.join(destpath, file), 'wb+') as f:
            self.ftp.retrbinary('RETR ' + file, f.write)
        self.__logout()

    def md5(self, sourcepath, file):

        self.__login()
        m = hashlib.md5()
        self.ftp.cwd(f"{self.rootpath}/{sourcepath}")
        self.ftp.retrbinary('RETR %s' % file, m.update)
        md5_hash = m.hexdigest()
        self.__logout()

        return md5_hash


def download_all(ftp_instance, folder, destination):
    for file in ftp_instance.ls(folder):
        ftp_instance.download(folder, destination, file)
