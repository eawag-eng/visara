#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Core: Data handling tools.
# ==============================================================================
from visara import LOGGER
from visara.core.errors import ConfigurationError

from pathlib import Path
from datetime import datetime
import hashlib
from os.path import exists

import pandas as pd

from visara.core.db_tools import DataBase
from visara.core.ftp import FtpUtil


def create_local_folder_if_not_exists(folder):
    path = Path(folder)
    if not path.exists():
        path.mkdir()


def hash_local(filename):
    hash_md5 = hashlib.md5()
    with open(filename, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def analyse_local(local_folder):
    folder = Path(local_folder)
    local_files = {}
    for file in folder.iterdir():
        if file.is_file():
            local_files[file.name] = hash_local(folder / file)
    return local_files


def analyse_remote(remote_details, ftp_instance=None, start_patterns_to_ignore=(".",)):

    if ftp_instance is None:
        ftp_instance = FtpUtil(**remote_details)
    remote_files = {}
    for rm_file in ftp_instance.ls():
        if not rm_file == "." and not rm_file == ".." and Path(rm_file).suffix:
            remote_files[rm_file] = ftp_instance.md5("/", rm_file)

    to_delete = []
    for f, h in remote_files.items():
        for pattern in start_patterns_to_ignore:
            if f.startswith(pattern):
                to_delete.append(f)
                break

    for key in to_delete:
        del remote_files[key]

    return remote_files


def determine_files_to_copy(local_files, remote_files):
    to_copy = []

    for file_on_ftp in remote_files.keys():
        if file_on_ftp in local_files.keys():
            if local_files[file_on_ftp] != remote_files[file_on_ftp]:
                to_copy.append({
                    "name": file_on_ftp,
                    "status": "changed"
                })
        else:
            to_copy.append({
                "name": file_on_ftp,
                "status": "new"
            })
    return to_copy


def synchronize_with_ftp(local_folder, remote_details, ftp_instance=None, patterns_to_ignore=(".",)):
    LOGGER.info("Ftp server -> local storage.")

    LOGGER.info("Analyzing local.")
    local_files = analyse_local(local_folder)
    LOGGER.info("Analyzing remote.")
    remote_files = analyse_remote(remote_details, ftp_instance, patterns_to_ignore)
    files = determine_files_to_copy(local_files, remote_files)

    if files:
        LOGGER.info("Downloading files")
        if ftp_instance is None:
            ftp_instance = FtpUtil(**remote_details)

        for file in files:
            LOGGER.info(f'... {file["name"]}')
            ftp_instance.download("/", local_folder, file["name"])

    return files


def init_database_and_table_if_not_exists(database, query):

    path = Path(database)
    if not path.exists():
        db = DataBase(database)
        response = db.query(
            query,
            commit=True,
            show=False
        )
        return response


def create_renamed_columns(old_columns):
    signs_to_replace = {
        "-": "_",
        ".": "_",
        " ": "_",
        "/": "_",
        "[": "",
        "]": "",
        "(": "",
        ")": "",
        "%": "percent",
        "°": ""
    }
    to_replace = {}
    for col in old_columns:
        old_col = col
        for sign, new_sign in signs_to_replace.items():
            col = col.replace(sign, new_sign)
        to_replace[old_col] = col

    return to_replace


def parse_file(file, sep=",", date_col="Date", time_col="Time", datetime_col="", date_time_format="%d.%m.%Y %H:%M:%S"):

    df = pd.read_csv(file, sep=sep, header=0)

    if (datetime_col and time_col) or (datetime_col and date_col) or (date_col and not time_col) or (time_col and not date_col):
        raise ConfigurationError(
            "You must either provide the name of the datetime column or"
            " if the columns are separated the name of the date column and the name of the time column!"
            f" You provided: time_col '{time_col}', date_col '{date_col}' and datetime_col '{datetime_col}'."
        )

    columns_to_drop = [col for col in df.columns if "Unnamed" in col]

    if datetime_col:
        df["Datetime"] = pd.to_datetime(df[datetime_col], format=date_time_format)
        if datetime_col != "Datetime":
            columns_to_drop += [datetime_col]
    elif date_col and time_col:
        df["Datetime"] = pd.to_datetime(df[date_col] + " " + df[time_col], format=date_time_format)
        columns_to_drop += [date_col, time_col]

    df.drop(columns=columns_to_drop, inplace=True)

    to_replace = create_renamed_columns(df.columns)

    df.rename(columns=to_replace, inplace=True)

    df = df[~df["Datetime"].duplicated()]
    df.sort_values(by="Datetime")

    return df


def match_pandas_to_sqlite_types(data):
    # maps pandas to sqlite types
    types = {
        "int64": "integer",
        "float64": "real",
        "datetime64[ns]": "datetime",
        "object": "text"
    }

    col_types_sqlite = {}
    for col, typ in dict(data.dtypes).items():
        col_types_sqlite[col] = types[str(typ)]

    return col_types_sqlite


def generate_table_query_from_col_type_map(col_type_map, table_name="visara"):
    base_query = f"CREATE TABLE IF NOT EXISTS {table_name} ("
    for column, typ in col_type_map.items():
        base_query += f"{column} {typ},"

        if typ == "datetime":
            base_query = base_query[:-1]
            base_query += " UNIQUE,"

    base_query = base_query[:-1]
    base_query += ");"

    return base_query


def generate_table_creation_query(data, table_name="visara"):
    col_types_sqlite = match_pandas_to_sqlite_types(
        data
    )

    return generate_table_query_from_col_type_map(col_types_sqlite, table_name)


def get_last_row_from_database(database, table_name="visara", datetime_col="Datetime"):

    if not exists(database):
        raise FileExistsError(f"The database '{database}' does not exist.")

    db = DataBase(database)
    return db.query(
        f"SELECT {datetime_col} FROM {table_name} ORDER BY {datetime_col} DESC LIMIT 1;"
    )


def fill_database(data, database, table_name="visara", datetime_col="Datetime"):

    if not exists(database):
        raise FileExistsError(f"The database '{database}' does not exist.")

    last = get_last_row_from_database(database, table_name, datetime_col)
    if last:
        # filter dataframe
        data = data.loc[data.Datetime > last[0][0], :]

    with DataBase(database) as connection:
        data.to_sql(
            name=table_name,
            con=connection,
            index=False,
            if_exists="append"
        )

    if data.empty:
        return []
    else:
        return data.to_dict(orient="records")


def sort_files(file_list, file_name_pattern):
    return sorted(file_list, key=lambda x: datetime.strptime(x.name, file_name_pattern))


def list_and_sort_files(folder, file_name_pattern):
    files = []
    path = Path(folder)
    for file in path.iterdir():
        if file.is_file() and file.suffix != ".sqlite":
            files.append(file)
    return sort_files(files, file_name_pattern)


def delete_database(database):
    db = Path(database)
    if db.exists():
        db.unlink()


def init_folders_and_database(folders, database, query):
    for folder in folders:
        create_local_folder_if_not_exists(folder)

    init_database_and_table_if_not_exists(database, query)


def update_database(data_folder, database, return_filled=False, file_name_pattern="logfile_%d%m%Y.csv", table_name="visara", **file_parser_args):
    LOGGER.info("Local storage -> database!")

    filled = []
    for file in list_and_sort_files(data_folder, file_name_pattern):
        data = parse_file(file, **file_parser_args)
        filled_data = fill_database(data, database, table_name)

        if return_filled:
            filled += filled_data

    if return_filled:
        return filled


def read_database(database, time_start, time_end, table_name="visara", datetime_col="Datetime"):
    LOGGER.info("Reading database!")

    if not exists(database):
        raise FileExistsError(f"The database '{database}' does not exist.")

    query = f"SELECT * FROM {table_name} WHERE {datetime_col} " \
            f"BETWEEN '{time_start}' AND '{time_end}';"

    with DataBase(database) as connection:
        df = pd.read_sql(
            query,
            connection,
            index_col=datetime_col,
            parse_dates=[datetime_col]
        )
        return df


def save_data(data, save_path):
    LOGGER.info("Saving data!")

    if data.empty:
        file_name = f"data_empty.csv"
    else:
        st = data.index.min().strftime("%Y%m%d%H")
        en = data.index.max().strftime("%Y%m%d%H")
        file_name = f"data_{st}_{en}.csv"

    data.to_csv(
        Path(save_path) / file_name,
        sep=";"
    )
