#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Core: Datetime parser
# ==============================================================================
from visara.core.errors import InvalidInformationError

from datetime import (
    datetime,
    timedelta
)


def format_output(dt, final_format="%F %X"):
    if isinstance(dt, datetime):
        return dt.strftime(final_format)
    else:
        fmt_to_try = [
            "%Y-%m-%d",
            "%Y-%m-%d %H",
            "%Y-%m-%d %H:%M",
            "%Y-%m-%d %H:%M:%S"
        ]
        for fmt in fmt_to_try:
            try:
                dt = datetime.strptime(dt, fmt)
                return dt.strftime(final_format)
            except ValueError:
                pass

        raise InvalidInformationError(f"The time string '{dt}' you provided is not valid!")


def parse_datetime(datetime_str):
    try:
        datetime_str = datetime_str.strip(" ")
        now = datetime.now()
        if datetime_str == "now":
            return format_output(now)
        elif "day" in datetime_str:
            dt = timedelta(
                days=abs(int(datetime_str.strip("day")))
            )
            return format_output(now - dt)
        elif "hour" in datetime_str:
            dt = timedelta(
                hours=abs(int(datetime_str.strip("hour")))
            )
            return format_output(now - dt)
        else:
            return format_output(datetime_str)
    except Exception as e:
        print(f"Please check your configuration entry '{datetime_str}'. Tis format is not known.")
        raise e
