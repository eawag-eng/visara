#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Core: Basic memory functions. (ROM)
# ==============================================================================
from visara import LOGGER

from pathlib import Path


def delete_memory(workdir, memory_id):

    memory_file = Path(workdir) / f"{memory_id.lower()}.memory"

    if memory_file.exists():
        memory_file.unlink()


def read_memory(workdir, memory_id):
    memory_file = Path(workdir) / f"{memory_id.lower()}.memory"

    if not memory_file.exists():
        memory_file.touch()

    return memory_file.read_text().split("\n")[:-1]


def write_memory(workdir, memory_id, file_list):
    memory_file = Path(workdir) / f"{memory_id.lower()}.memory"

    with memory_file.open("a") as f:
        f.write("\n".join(file_list) + "\n")


def set_ini_file_location(location, filename=".visara_info"):
    LOGGER.info("Setting default ini file location.")
    home_dir = Path().home()
    visara_file = home_dir / filename
    if visara_file.exists():
        visara_file.touch()

    visara_file.write_text(str(Path(location).absolute()))


def get_ini_file_location(filename=".visara_info"):
    LOGGER.info("Getting default ini file location.")
    visara_file = Path().home() / filename

    if visara_file.exists():
        return visara_file.read_text()
    else:
        return ""


def delete_ini_file_location(filename=".visara_info"):
    LOGGER.info("Removing default ini file location.")
    visara_file = Path().home() / filename
    if visara_file.exists():
        visara_file.unlink()


def choose_ini_file(filename=""):
    LOGGER.info("Choose ini file.")
    if filename == "":
        filename = get_ini_file_location()

        if filename == "":
            raise TypeError(
                "You must set an ini file! Use the commandline interface as described in the documentation!")

    if not Path(filename).exists():
        raise FileNotFoundError(f"The file '{filename}' does not exist.")

    return filename