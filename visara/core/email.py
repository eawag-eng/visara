#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Core: Email class for notifying people.
# ==============================================================================
import os
import ssl
import smtplib
from io import StringIO
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class EMail:
    """
    Example
    ------
    from visara.core.email import EMail

    email = EMail(
        dict(host="smtp.gmail.com", port=465, pwd="password", sender="visara.info@gmail.com"),
        recipient="christian.foerster@eawag.ch",
        subject="test",
        message="some text",
        filename="/home/heinrich/Downloads/performance test.ipynb"
    )
    email.send()
    """

    def __init__(self, connection, recipient, subject, message, filename=None):

        self.__host = connection["host"]
        self.__port = connection["port"]
        self.__pwd = connection["pwd"]
        self.__sender = connection["sender"]

        self._msg = MIMEMultipart()
        self._msg['Subject'] = subject
        self._msg['From'] = self.__sender
        self._msg['To'] = recipient if not isinstance(recipient, list) else ','.join(recipient)

        self._msg.attach(MIMEText(message, "plain"))

        # Open file in binary mode
        if filename is not None:
            attachment = EMail._attach(filename)

            self._msg.attach(attachment)
        self._msg['text'] = self._msg.as_string()



    @classmethod
    def _attach(cls, filename):
        with open(filename, "rb") as file:
            attachment = MIMEBase("application", "octet-stream")
            attachment.set_payload(file.read())

        encoders.encode_base64(attachment)

        attachment.add_header(
            "Content-Disposition",
            f"attachment; filename= {os.path.split(filename)[-1]}",
        )

        return attachment

    def send(self):
        context = ssl.create_default_context()
        server = smtplib.SMTP_SSL(self.__host, self.__port, context=context)
        server.login(user=self._msg['From'], password=self.__pwd)
        server.sendmail(self._msg['From'], self._msg['To'], self._msg['text'])
        server.quit()


def send_mail(connection, recipient, subject="VisARA Notification", message=None, filename=None, error_message=None):

    if isinstance(error_message, StringIO):
        error_message = error_message.getvalue()

    if error_message is not None and message is None:
        message = f"An error has occurred! Please inform the system admin!\n{error_message}"

    elif error_message is not None and message is not None:
        message = f"{message}\n\n{error_message}"

    e = EMail(
        connection,
        recipient,
        subject,
        message,
        filename=filename
    )
    e.send()
