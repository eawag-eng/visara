#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Core: Plotting functions and helpers
# ==============================================================================
from visara.core.data_functionality import create_renamed_columns
from visara.core.errors import ConfigurationError, InvalidInformationError
from visara.core.config import str_is_float
from visara import LOGGER

import plotly.graph_objects as go
import pandas as pd

import os


def getting_mapping_values(config, section_prefix="CHANNEL_TO_VALUE"):

    mapping = {}
    for section, values in config.items():
        if section == section_prefix:
            mapping.update(values)

    #if not mapping:
    #    raise ConfigurationError("No 'channel to value definition' found in configfile!")

    return mapping


def get_group_column(config, section_prefix="CHANNEL_TO_VALUE"):
    mapping = getting_mapping_values(config, section_prefix)
    assert len(mapping) == 1, "You have multiple Channel to Value mappings only one is allowed!"
    return list(mapping)[0]


def match_col_name_to_channel_name(columns, _column):
    col_name = ""
    for col in columns:
        if col.lower() == _column.lower():
            col_name = col

    if not col_name:
        raise InvalidInformationError(
            f"No column that matches your mapping channel name '{_column}' could be found."
        )

    return col_name


def match_replace_types(column, mapp):
    type_col = column.dtype
    key_from_map = list(mapp)[0]
    type_map = type(key_from_map)

    if isinstance(key_from_map, str):
        if str_is_float(key_from_map):
            raise ConfigurationError(f"Floating point keywords '{key_from_map}' are forbidden!")

    if not (type_col == type_map):

        if str(column.dtype) != "object":
            column = column.astype(int)
        column = column.astype(type_map)

    return column


def map_channels_to_values(data, mapping_dict):
    LOGGER.info("Mapping channels to values.")

    columns = data.columns
    for channel, mapp in mapping_dict.items():
        col_name = match_col_name_to_channel_name(columns, channel)
        data[col_name] = match_replace_types(
            data[col_name], mapp
        ).replace(
            to_replace=mapp
        )

    return data


def get_columns_to_plot(columns, exclude_columns):
    if exclude_columns:

        if not isinstance(exclude_columns, list):
            exclude_columns = [exclude_columns]

        exclude_columns = list(create_renamed_columns(exclude_columns).values())

        return [col for col in columns if col not in exclude_columns]

    else:
        return columns


def reindex(data, new_dt=None):
    """
    new_dt : datetime.timedelta
    """
    if not new_dt:
        new_dt = data.index.to_series().diff().min()

    start = data.index.min()
    end = data.index.max()
    ts = pd.date_range(start, end, freq=new_dt)

    return data.reindex(ts)


def resample(data, accumulate=""):
    if accumulate:
        if isinstance(data, pd.DataFrame):
            if all([_type != "object" for _type in data.dtypes]):
                data = data.resample(accumulate).mean()
        elif isinstance(data, pd.Series):
            if data.dtype != "object":
                data = data.resample(accumulate).mean()
    return data


def plot_data(data, group_by_column, exclude_columns, accumulate,
              save_path, color_map, line_width=1, marker_size=2, identifier="", parameter_unit_map={}):

    if identifier:
        identifier += "_"

    group_by_column = match_col_name_to_channel_name(
        data.columns,
        group_by_column
    )

    columns = get_columns_to_plot(
        data,
        exclude_columns=exclude_columns
    )

    data[group_by_column] = match_replace_types(
        data[group_by_column], color_map
    )

    message = ""
    if data.empty:
        message = "empty "
        LOGGER.info(
            f"The start time and end time you provided does not contain any data."
        )

    LOGGER.info(f"Creating {message}html files.")
    for column in columns:
        fig = go.Figure()
        for (channel, group) in data.groupby(group_by_column):
            group = group[column]
            # group = reindex(group)
            group = resample(group, accumulate)
            try:
                fig.add_trace(
                    go.Scatter(
                        x=group.index,
                        y=group.values,
                        mode='lines+markers',
                        name=channel,
                        line=dict(
                            color=color_map[channel],
                            width=line_width
                        ),
                        marker=dict(
                            size=marker_size,
                            color=color_map[channel]
                        )
                    )
                )
            except KeyError as e:
                print("There is an unknown channel in your data! Mind the key given in the error message!")
                raise e

        fig.update_layout(
            title={
                'text': column,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top'
            }
        )
        fig.update_xaxes(
            title_text="Time",
            # rangeslider_visible=True,
            # rangeslider_thickness=0.07
        )
        if column in parameter_unit_map:
            fig.update_yaxes(title_text=f"{column} {parameter_unit_map[column]}")

        fig.update_layout(showlegend=True)

        fig.write_html(
            os.path.join(save_path, f"{identifier}{column}.html")
        )
