#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Core: Database tool to communicate with SQLite
# ==============================================================================
import sqlite3
from visara import LOGGER


class DataBase:
    """Manages database connection and creation along with executing SQL Queries"""
    def __init__(self, db_file):
        """Takes a /path/to/an/example.db string"""
        self.db_file = db_file

        try:
            self._open()
            self._close()
        except Exception as e:
            raise e

    def __enter__(self):
        self._open()
        return self.connection

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self._close()

    def _open(self):
        """Creates a connection to the database"""
        try:
            self.connection = sqlite3.connect(self.db_file)
            self._cur = self.connection.cursor()
        except Exception as e:
            LOGGER.exception(e)
            raise e

    def _close(self):
        """Closes the database"""
        self._cur.close()
        self.connection.close()

    def query(self, query_str, commit=False, show=False):
        """Connects to the database and executes a query """
        self._open()

        query_str = query_str.rstrip()

        if ";" not in query_str:
            query_str = query_str + ";"

        if show:
            print(query_str)

        self._cur.execute(query_str)
        data = self._cur.fetchall()
        
        if commit:
            self.connection.commit()

        self._close()
        
        return data

