#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Core: Generate a config file to set up your visara program
# ==============================================================================
from visara import LOGGER

from textwrap import dedent
from pathlib import Path

__INI = dedent(
    """
    #######################################
    ##        --- IMPORTANT ---          ##
    ##                                   ##
    ## time (plot and export sections)   ## 
    ## -------------------------------   ##
    ## allowed formatting:               ##
    ## now -> ...now...                  ##
    ## -n day -> now minus n days        ##
    ## -n hour -> now minus n hours      ##
    ## Y-m-d -> eg.: 2020-01-23          ##
    ## Y-m-d H-> eg.: 2020-01-23 12      ##
    ## Y-m-d H:M-> eg.: 2020-01-23 12:00 ##
    ##                                   ###################################################################
    ##                                                                                                    ##
    ## When using percent signs the must be escaped!!                                                     ##
    ## So you must always use two to express datetime patterns!!                                          ##
    ##                                                                                                    ##
    ##                                                                                                    ##
    ## accumulation (for plotting section)                                                                ## 
    ## -----------------------------------                                                                ##
    ## if you want to plot accumulated (mean) signals following options to accumulate are valid           ##
    ## https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#timeseries-offset-aliases  ##
    ## Examples:                                                                                          ##
    ## 1min -> accumulates minutely                                                                       ##
    ## 5min -> accumulate 5 minutely                                                                      ##
    ## 1H -> accumulate hourly                                                                            ##
    ## 30S -> accumulate 30 secondly                                                                      ##
    ## !! if no value is entered no accumulation !!                                                       ##
    ##                                                                                                    ##
    ## colors (for plotting section)                                                                      ##
    ## -----------------------------                                                                      ##
    ## colors must be specified in hexadecimal format!! (looks like this:  #d0d3d4)                       ##
    ## you can find colors here https://htmlcolorcodes.com/ or https://coolors.co/palettes/trending       ##
    ########################################################################################################
        
    [DATABASE]
    # will be generated in data_directory (under the PATHS section)
    database_file = database.sqlite
    
    # please adjust to your wishes 
    table_name = visara
    
    [FILE_SETTINGS]
    # specify the filename pattern
    file_name_pattern = logfile_%%d%%m%%Y.csv
    
    # how are the columns separated
    delimiter = ,
    
    # please either fill in 'date_col_name' and 'time_col_name' if the columns are separated 
    # OR 
    # 'datetime_col_name' if the datetime information is on one column
    date_col_name = Date
    time_col_name = Time
    datetime_col_name = 
    
    # First enter the date and the time format separated by a space
    date_time_format= %%d.%%m.%%Y %%H:%%M:%%S
    
    [CHANNEL_TO_VALUE]
    # mapping the channel numbers wth column name 'Valve_ai' to values
    Valve_ai=
        1 = Bio1_G1
        2 = Bio1_G2
        3 = Bio1_G3
        4 = Bio1_G4
        5 = Bio2_V1
        6 = Bio2_V2
        7 = Bio2_V3
        8 = Bio2_V4
        9 = Abluft_Bio1
        10 = Abluft_Bio2
        11 = Kalibration_N2O
        12 = Kalibration_CH4
        14 = Aussenluft
        16 = Kalibration_N2
        19 = Abluft_Mech_R
        20 = Abluft_Biofilter
        21 = Abluft_ANMX
        22 = Abluf_Deodor
    
    [FTP]
    # please fill in the connection to the sps ftp server
    host = 192.168.1.100
    usr = admin
    pwd = wago
    path = /media/SD/CSV_Files
    
    [PATHS]
    root_directory = C:/visara
    data_directory = %(root_directory)s/data
    plot_directory = %(root_directory)s/plot
    export_directory = %(root_directory)s/export
    logging_directory = %(root_directory)s/logging
    
    [EXPORT_DEFAULT]
    time_start = -2 day
    time_end = now
    
    [EXPORT_ALL]
    time_start = 1970-01-01
    time_end = now
    
    [PLOT_DEFAULT]
    time_start = -1 day
    time_end = now
    
    identifier = VisARA 
    
    # pass multiple columns as list
    # eg.: [No., Value]
    exclude_columns = No.
    
    parameter_to_unit = 
        Flow_rSig = [l/min]
        O2_rSig = [%%]
        N2O_rSig = [ppm]
        NO_rSig = [ppm]
        CH4_rSig = [ppm]
    
    accumulate =
    
    line_width = 1
    marker_size = 1
    colors =
        Bio1_G1 = #ffbe0b
        Bio1_G2 = #fb5607
        Bio1_G3 = #ff006e
        Bio1_G4 = #8338ec
        Bio2_V1 = #3a86ff
        Bio2_V2 = #335c67
        Bio2_V3 = #fff3b0
        Bio2_V4 = #e09f3e
        Abluft_Bio1 = #9e2a2b
        Abluft_Bio2 = #540b0e
        Kalibration_N2O = #386641
        Kalibration_CH4 = #6a994e
        Aussenluft = #a7c957
        Kalibration_N2 = #bc4749
        Abluft_Mech_R = #ffbe0b
        Abluft_Biofilter = #fb5607
        Abluft_ANMX = #ff006e
        Abluf_Deodor = #ff006e
    
    [PLOT_30D]
    time_start = -30 day
    time_end = now
    
    identifier = VisARA
    
    # pass multiple columns as list
    # eg.: [No., Value]
    exclude_columns = No.
    
    parameter_to_unit = 
        Flow_rSig = [l/min]
        O2_rSig = [%%]
        N2O_rSig = [ppm]
        NO_rSig = [ppm]
        CH4_rSig = [ppm]
        
    accumulate =1min
    
    line_width = 1
    marker_size = 1
    colors =
        Bio1_G1 = #ffbe0b
        Bio1_G2 = #fb5607
        Bio1_G3 = #ff006e
        Bio1_G4 = #8338ec
        Bio2_V1 = #3a86ff
        Bio2_V2 = #335c67
        Bio2_V3 = #fff3b0
        Bio2_V4 = #e09f3e
        Abluft_Bio1 = #9e2a2b
        Abluft_Bio2 = #540b0e
        Kalibration_N2O = #386641
        Kalibration_CH4 = #6a994e
        Aussenluft = #a7c957
        Kalibration_N2 = #bc4749
        Abluft_Mech_R = #ffbe0b
        Abluft_Biofilter = #fb5607
        Abluft_ANMX = #ff006e
        Abluf_Deodor = #ff006e
    
    [EMAIL]
    host = 
    port = 
    pwd = 
    sender = 
    
    [NOTIFICATIONS]
    verbose = True
    debug = False
    log_file = visara.log
    # multiple receivers should be seperated by commas
    # the email section must be filled in, fi you want to use the email functionality!
    email = 
    
    """
)


def generate_conf(path):
    LOGGER.info(f"Generating config file '{path}'!")
    p = Path(path).absolute()
    p.write_text(
        __INI
    )
