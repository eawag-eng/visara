#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Main: Initializing logger streams
# ==============================================================================
from visara.core.logger import MainLogger, get_logger
from visara.core.email import send_mail

from io import StringIO
import os

LOGGER = MainLogger()

EMAIL_STREAM = StringIO()
EMAIL_LOGGER = get_logger(EMAIL_STREAM)


def log_error(config, err, subject="VisARA Notification", message=None):
    LOGGER.add_log_file(
        log_file=os.path.join(
            config["PATHS"]["logging_directory"],
            config["NOTIFICATIONS"]["log_file"]
        )
    )
    LOGGER.exception(err)

    if config["NOTIFICATIONS"]["email"]:
        EMAIL_LOGGER.exception(err)
        send_mail(
            config["EMAIL"],
            recipient=config["NOTIFICATIONS"]["email"],
            subject=subject,
            message=message,
            error_message=EMAIL_STREAM
        )