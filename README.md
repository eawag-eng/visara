﻿# VisARA

Data visualization for your **N2OARA** project.

Program capabilities:

- synchronizing ftp server to local storage and database
- visualizing data from database
- exporting data from database

## Installation

The program installation and set up will need 3 steps. 

1. install **Python** and ideally **Git**
2. install the **VisARA** package
3. set up **VisARA**

Please follow theses detailed instructions:

### Install Python and Git
1. Install [miniconda 64 bit](https://docs.conda.io/en/latest/miniconda.html) 
   (**select add to _PATH_ option during installation**)
   
2. Install [Git 64 bit](https://git-scm.com/download/win)  (all default)

> Check if **Python** and **Git** installation was successful!
> > **Python:** 
> > Type `python` in a terminal. That should look something like this.
> > ```
> > C:/Users/YourUserName> python
> > Python 3.8.1 (default, Jan  8 2020, 22:29:32) 
> > [GCC 7.3.0] :: Anaconda, Inc. on linux
> > Type "help", "copyright", "credits" or "license" for more information.
> > >>>
> > ```
> > To exit the python interpreter type and hit enter:
> > ```
> > >>> exit()
> > ```
> 
> >  **Git:**
> > Right click into any folder in the file explorer and check for the option: **Git Bash**.

> **Note:** If you had issues with the python installation, 
> please use the **Anaconda Prompt** as terminal for all other steps!

### Install VisARA:

1. Clone the git repository by running the following command in the git bash.
```shell
git clone https://gitlab.switch.ch/eawag-eng/visara.git
```
2. Install the **VisARA** package
```shell
pip install -e visara
```

> Check if **VisARA** installation was successful!
> 
> Type `python -m visara` in the terminal. That should look something like this.
> ```
> C:/Users/YourUserName> python -m visara
> usage: visara [-h] {config,control,synchronize,plot,export} ...
> 
> Set set your default configuration, synchronize, plot, export data and more.
> 
> positional arguments:
>   {config,control,synchronize,plot,export}
>     config              Save the location of a default / or generate a config file.
>     control             Basic controls for initiation and resetting the database.
>     synchronize         Synchronize ftp server with local files and database.
>     plot                Visualize data from database.
>     export              Save data from database to csv file.
> 
> optional arguments:
>   -h, --help            show this help message and exit
> ```
> 


### Set up VisARA

Once you have checked the visara installation worked,
you need to generate a configuration file and set a few parameters.


#### Generate
To generate a configuration file please type in a terminal.
```shell
python -m visara config --generate "C:/path/to/where/config/should/be/generated/config_name.ini"
```

#### Adjust settings
Now you can open the config file in a text editor. It consists of different section, with attributes and values.
> **Example:**
> ```ini
> [SECTION_1]
> 
> attribute_1 = value_1
> attribute_2 = value_2
> ...
> ```

**The most important settings you need to adjust are the following:** 
> ```ini
> [FILE_SETTINGS]
> ...
> 
> [CHANNEL_TO_VALUE]
> ...
> 
> [FTP]
> ...
> 
> [PATHS]
> ...
> 
> [PLOT_DEFAULT]
> ...
> ```

**Below is an examples for setting the config file depending on your system!**

> **Example:**
> 
> > **--- NECESSARY INFOS ---**  
> > 
> > Filenames on ftp server are:  
> > **abc-2021010100.csv**, **abc-2021010112.csv**, **abc-2021010200.csv**
> > 
> > The file's content looks like this:  
> > **No.,DateTime,Channel_No,N2O_PlotMe,NO_PlotMe,O2_PlotMe,CH4_PlotMe,Flow_PlotMe,**   
> > **1,26.06.2020 00:00:00,1.00,0.00,0.00,17.32,55.33,0.85,**  
> > **...**
> > 
> > Ftp server details:  
> > **host_ip=192.168.1.44, username=Max, password=secret, path_to_where_the_data_lies=/data_sps/**
> > 
> > Path details:  
> > **VisARA data is supposed to be stored at C:/visara** (You must create this folder!)
> >
> > Plot details:  
> > **Signals 1 to 3 are supposed to be _green_, _blue_ and _orange_**   
> > **File names should have the prefix _VisARA_**  
> > **The line width is supposed to be a bit stronger (value 2)**  
> > **The last 24 hours are supposed to be plotted**
> > 
> > Export details:
> > **A additional export profile is supposed to be configured to export the last 21 days!**
> 
> > **--- Correct File Settings ---**
> > ```ini
> > [FILE_SETTINGS]
> > # this section is necessary for the file reader to understand your files
> > file_name_pattern = abc-%%Y%%m%%d%%H.csv
> > delimiter = ,
> > # date_col and time_col are not separate columns that's why we have to fill in the 
> > # datetime_col_name value and leave the other two blank
> > date_col_name = 
> > time_col_name = 
> > datetime_col_name = DateTime
> > date_time_format= %%d.%%m.%%Y %%H:%%M:%%S
> > 
> > [CHANNEL_TO_VALUE]
> > # This section needs to be filled in on however you wired your sps.
> > # Channel_No is the name of the column that contains the channel id!
> > Channel_No = 
> >     1 = Signal_Name_1
> >     2 = Signal_Name_2
> >     3 = Signal_Name_3
> > 
> > [FTP]
> > # please fill in the ftp server connection details
> > host = 192.168.1.44
> > usr = Max
> > pwd = secret
> > path = /data_sps
> >
> > [PATHS]
> > # this section contains details on where exported data, plots, ftp files, 
> > # the database and the log files are kept.
> > # make sure the root directory (C:/visara) exists!
> > root_directory = C:/visara
> > data_directory = %(root_directory)s/data
> > plot_directory = %(root_directory)s/plot
> > export_directory = %(root_directory)s/export
> > logging_directory = %(root_directory)s/logging
> > 
> > [PLOT_DEFAULT]
> > # This is the default plot section, which will be used, 
> > # if no other section is passed by the user.
> > 
> > # plot the last 24 hours
> > time_start = -1 day
> > time_end = now
> > 
> > # beginning of filename of the generated html plots
> > identifier = VisARA 
> > 
> > # the wording that all columns you want to plot have in common
> > columns_to_plot_pattern = PlotMe
> >
> > # specify the unit of each column to be plotted on the y axis
> > parameter_to_unit = 
> >     Flow_PlotMe = [l/min]
> >     O2_PlotMe = [%%]
> >     N2O_PlotMe = [ppm]
> >     NO_PlotMe = [ppm]
> >     CH4_PlotMe = [ppm]
> > 
> > # perform a 10 second signal aggregation
> > accumulate = 10S
> > 
> > # width of lines on plot
> > line_width = 2
> > 
> > # specify the colors of each signal as hexadecimal string
> > # hexadecimal string can be found here https://coolors.co/palettes/trending
> > # green = #2a9d8f
> > # blue = #3d405b
> > # orange = #fb8500
> > colors =
> >     Signal_Name_1 = #2a9d8f
> >     Signal_Name_2 = #3d405b
> >     Signal_Name_3 = #fb8500
> > 
> > [EXPORT_21D]
> > # this export section is specified to export the last 21 days and is called:
> > # EXPORT_21D
> > time_start = -21day
> > time_end = now
> > ```


> ### Important Notes for Adjusting Config File:
> 
> > ### _Saving plot and export profiles!_
> >
> > It is vital that you adjust the settings file to your needs. You **may not** change **section** or **attribute names**!  
> > However, you can add or change **section names** for **plot** and **export sections**, 
> > except of the `PLOT_DEFAULT` and `EXPORT_DEFAULT`. These section names must stay as is!
> >
> > This enables you to save as many **plot** and **export** profiles as you want!
> > To **plot** and **export** data you simply provide the desired profile's section name you decided upon.  
> > **Please do not use whitespaces in section names!!**
> 
> > ### _Allowed time formatting_
> > 
> > **now** -> will be translated to the current time                
> > **-n day** -> now minus *n* days (*n* must be an integer)        
> > **-n hour** -> now minus *n* hours (*n* must be an integer)    
> > **Y-m-d** -> eg.: 2020-01-23         
> > **Y-m-d H** -> eg.: 2020-01-23 12     
> > **Y-m-d H:M** -> eg.: 2020-01-23 12:00
> 
> > ### _Data accumulation arguments_
> >  
> > This is an option available for plots! Especially when saving long time frames this speed up the plotting progress and keep the interactive plot fast and responsive!!
> > 
> > Just leave the field '_accumulate_' blank, if you do not want any accumulation.
> >
> > **1min** -> accumulates minutely  
> > **5min** -> accumulate 5 minutely  
> > _n_**min** -> accumulate _n_ minutely (*n* must be an integer)  
> > **1H** -> accumulate hourly  
> > _n_**H** -> accumulate _n_ hourly (*n* must be an integer)  
> > **30S** -> accumulate 30 secondly  
> > _n_**S** -> accumulate _n_ secondly (*n* must be an integer)  

For convenience, it is recommended to set the location for your default configuration file. 
If you do not do this, you must provide the location of the config file for each command you run!
```shell
python -m visara config --set "C:/path/to/where/config/should/be/generated/config_name.ini"
```

Next you have to initiate the program, that will create a few folders and set up the database!
```shell
python -m visara control --init "C:/path/to/sample/csv/file"
```

## Using VisARA

VisARA has a **C**ommand **L**ine **I**nterface (**CLI**) which dann be used to easily
configure the program synchronize the ftp server to the database, plot and export data.

### Synchronize Data
Perform synchronisation of ftp server to database.
```shell
python -m visara synchronize --now
```

### Plot Data
Plot data with default _plot profile_!
```shell
python -m visara plot --now
```

Plot data with _plot profile_ called `PLOT_14D_ACC_1MIN`!
```shell
python -m visara plot --now --section "PLOT_14D_ACC_1MIN"
```

### Export Data
Export data with _export profile_ called `EXPORT_FAVOURITE_14D`!
```shell
python -m visara export --now --section "EXPORT_FAVOURITE_14D"
```

Export data with _default export profile_ but map channels to signal names!
```shell
python -m visara export --now --mapped
```

Export data with _export profile_ called `EXPORT_21D` but map channels to signal names!
```shell
python -m visara export --now --mapped --section "EXPORT_21D"
```

### Help

If your unsure on how to use the **CLI**, you can use the help command to print useful information.

For instance getting information on the different visara controls works like this:
```shell
python -m visara --help
```
or getting infos on the plot controls like this:
```shell
python -m visara plot --help
```


### Additional CLI Flags

You can run any command with the `-v` or `--verbose` flag to get information from the program while you run it.  
For example:
```shell
python -m visara plot --now --verbose
```

### Email support

In order to be able to send emails you must provide all necessary info in the `EMAIL` section of the configuration file.
An example for a gmail email account would be: 
```ini
[EMAIL]
host = smtp.gmail.com
port = 465
pwd = your-very-secret-password
sender = your-email-address@gmail.com
```

## Setting up a scheduled task on Windows

https://www.windowscentral.com/how-create-automated-task-using-task-scheduler-windows-10


## Update the **VisAra** package:

Open the **Git Bash** in the previously cloned **VisARA** repository (folder) and run the commmand:
```shell
git pull
```

## Attention

> ### _Files must be consistent_
>  
> **Files** on the **ftp server** must all have the **same layout** containing the **same column names** and follow the same **naming convention**!

> ### _Folders are ignored_
>  
> Sub-folders of the main path you specified in the ftp server settings are ignored!


## Run Tests
Access VisARA repository (folder) with a commandline.

1. To install required packages run:
> ```shell
> pip install -r requirements_dev.txt
> ```

2. To run the tests type:
> Run tests without verbose output.
> ```shell
> pytest
> ```
> Run tests with verbose output.
> ```shell
> pytest -s
> ```
> To also test sending emails when errors occur. You must provide a valid receiving email address.   
> (YOU MUST HAVE FILLED OUT THE EMAIL SECTION IN THE CONFIG FILE!)
> ```shell
> pytest --run-email-test "your.email.address@domain.ch"
> ```


