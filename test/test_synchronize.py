#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: Synchronization tools.
# ==============================================================================
from visara.core.data_functionality import synchronize_with_ftp, determine_files_to_copy, analyse_local


def test_analyse_local(local_folder, generate_files):
    files = generate_files(local_folder, 0, 20)

    content = analyse_local(local_folder)

    assert len(content) == 20
    assert sorted(list(content)) == sorted(files)


def test_determine_files_to_copy_scenario_new_remote_files(local_folder, remote_folder, generate_files):

    local_files = generate_files(local_folder, 0, 20)
    remote_files = generate_files(remote_folder, 0, 30)

    local = analyse_local(local_folder)
    remote = analyse_local(remote_folder)

    to_copy = determine_files_to_copy(local, remote)

    files_to_copy_generated = sorted(list(set(remote_files)-set(local_files)))
    files_to_copy_analysed = sorted([file["name"] for file in to_copy])

    assert files_to_copy_generated == files_to_copy_analysed


def test_determine_files_to_copy_scenario_deleted_remote_files(local_folder, remote_folder, generate_files):

    local_files = generate_files(local_folder, 0, 20)
    # first 10 files removed
    remote_files = generate_files(remote_folder, 10, 30)

    local = analyse_local(local_folder)
    remote = analyse_local(remote_folder)

    to_copy = determine_files_to_copy(local, remote)

    files_to_copy_generated = sorted(list(set(remote_files)-set(local_files)))
    files_to_copy_analysed = sorted([file["name"] for file in to_copy])

    assert files_to_copy_generated == files_to_copy_analysed


def test_determine_files_to_copy_scenario_changed_remote_files(local_folder, remote_folder, generate_files):

    local_files = generate_files(local_folder, 0, 20)
    remote_files = generate_files(remote_folder, 0, 20)

    local = analyse_local(local_folder)
    remote = analyse_local(remote_folder)

    to_copy = determine_files_to_copy(local, remote)

    files_to_copy_generated = sorted(list(set(remote_files)-set(local_files)))
    files_to_copy_analysed = sorted([file["name"] for file in to_copy])

    assert files_to_copy_generated == []
    assert files_to_copy_analysed == []

    # changing file:
    (remote_folder / "19.csv").write_text(
        "aaa"
    )

    remote = analyse_local(remote_folder)
    to_copy = determine_files_to_copy(local, remote)

    assert to_copy == [{"name": "19.csv", "status": "changed"}]


def test_determine_files_to_copy_scenario_changed_local_files(local_folder, remote_folder, generate_files):

    local_files = generate_files(local_folder, 0, 20)
    remote_files = generate_files(remote_folder, 0, 20)

    local = analyse_local(local_folder)
    remote = analyse_local(remote_folder)

    to_copy = determine_files_to_copy(local, remote)

    files_to_copy_generated = sorted(list(set(remote_files)-set(local_files)))
    files_to_copy_analysed = sorted([file["name"] for file in to_copy])

    assert files_to_copy_generated == []
    assert files_to_copy_analysed == []

    # changing file:
    (local_folder / "19.csv").write_text(
        "aaa"
    )

    local = analyse_local(local_folder)
    to_copy = determine_files_to_copy(local, remote)

    assert to_copy == [{"name": "19.csv", "status": "changed"}]


def test_synchronized_with_ftp(ftpserver, get_ftp_tools, generate_files, local_folder, clean_up):
    local_ftp_path, ftp = get_ftp_tools(ftpserver)

    # generate files on ftp
    files = generate_files(local_ftp_path, 0, 10)
    synchronized_files = synchronize_with_ftp(local_folder, {}, ftp_instance=ftp)
    s_files = [f["name"] for f in synchronized_files]

    assert sorted(files) == sorted(s_files)

    (
        local_ftp_path / "0.csv"
    ).write_text(
        "new_content"
    )

    synchronized_files = synchronize_with_ftp(local_folder, {}, ftp_instance=ftp)

    assert synchronized_files == [{"name": "0.csv", "status": "changed"}]

    clean_up(local_ftp_path)

