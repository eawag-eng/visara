#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: Ftp server utility.
# ==============================================================================
from visara.core.data_functionality import hash_local


def test_ftp_ls(ftpserver, get_ftp_tools, generate_files, clean_up):
    local_ftp_path, ftp = get_ftp_tools(ftpserver)
    files = generate_files(local_ftp_path, 0, 10)
    read_files = ftp.ls()

    # ignoring .gitkeep file
    read_files = [f for f in read_files if not f.startswith(".")]

    assert read_files == files
    clean_up(local_ftp_path)


def test_ftp_get(ftpserver, get_ftp_tools, generate_files, clean_up):
    local_ftp_path, ftp = get_ftp_tools(ftpserver)
    files = generate_files(local_ftp_path, 0, 10)
    content = ftp.get("", "1.csv").read().decode("utf-8")
    assert content == "1"*100
    clean_up(local_ftp_path)


def test_ftp_download(ftpserver, get_ftp_tools, generate_files, clean_up, local_folder):
    local_ftp_path, ftp = get_ftp_tools(ftpserver)
    files = generate_files(local_ftp_path, 0, 10)
    ftp.download("", local_folder, "1.csv")
    assert (
        local_folder / "1.csv"
    ).read_text() == (
        local_ftp_path / "1.csv"
    ).read_text()
    clean_up(local_ftp_path)


def test_ftp_upload(ftpserver, get_ftp_tools, generate_files, clean_up, local_folder):
    local_ftp_path, ftp = get_ftp_tools(ftpserver)
    _ = generate_files(local_folder, 0, 10)
    ftp.upload(local_folder / "1.csv", "/")
    assert (
        local_folder / "1.csv"
   ).read_text() == (
        local_ftp_path / "1.csv"
   ).read_text()
    clean_up(local_ftp_path)


def test_ftp_md5(ftpserver, get_ftp_tools, generate_files, clean_up, local_folder):
    local_ftp_path, ftp = get_ftp_tools(ftpserver)
    files_local = generate_files(local_folder, 0, 10)
    files_ftp = generate_files(local_ftp_path, 0, 10)

    for file in files_local:
        assert (
            ftp.md5("", file)
        ) == hash_local(
            local_folder / file
        )

    clean_up(local_ftp_path)

