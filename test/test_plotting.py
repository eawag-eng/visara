#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: Different functions that are used for plotting
# ==============================================================================
from visara.core.config import read_config
from visara.core.plotting import (
    getting_mapping_values,
    map_channels_to_values,
    match_replace_types,
    plot_data,
    reindex,
    resample
)
from visara.core.errors import InvalidInformationError
from textwrap import dedent
import pandas as pd


def test_getting_mapping_values(tmp_path):
    ini = tmp_path / "config.ini"

    ini.write_text(
        dedent(
            """
           [CH_TO_VAL]
            VALVE_AI = 3 = Bio1_G3
                4 = Bio1_G4
                5 = Bio2_V1
            
            NO_RSIG = 
                1 = Bio1_G1
                2 = Bio1_G2
            """
        )
    )

    config = read_config(ini)
    maps = getting_mapping_values(
        config,
        section_prefix="CH_TO_VAL"
    )

    assert maps == {
        'valve_ai': {
            '3': 'Bio1_G3',
            '4': 'Bio1_G4',
            '5': 'Bio2_V1'
        }, 'no_rsig': {
            '1': 'Bio1_G1',
            '2': 'Bio1_G2'
        }
    }


def test_getting_mapping_values_empty(tmp_path):
    ini = tmp_path / "config.ini"

    ini.write_text(
        dedent(
            """
            [ABC]
            a=9
            
            [CH_TO_VAL]
            """
        )
    )

    config = read_config(ini)
    maps = getting_mapping_values(
        config,
        section_prefix="CH_TO_VAL"
    )

    assert maps == {}


def test_match_and_replace():
    df = pd.DataFrame([{
        "a": 1.0,
        "b": 1,
        "c": "d"
    }])

    dft = df.copy()
    dft.a = match_replace_types(dft.a, {1: "replaced"})
    dft = map_channels_to_values(df, {"a": {1: "replaced"}})
    assert dft.a.iloc[0] == "replaced"

    dft = df.copy()
    dft.a = match_replace_types(dft.a, {"1": "replaced"})
    dft = map_channels_to_values(df, {"a": {"1": "replaced"}})
    assert dft.a.iloc[0] == "replaced"

    dft = df.copy()
    dft.b = match_replace_types(dft.b, {1: "replaced"})
    dft = map_channels_to_values(df, {"b": {1: "replaced"}})
    assert dft.b.iloc[0] == "replaced"

    dft = df.copy()
    dft.b = match_replace_types(dft.b, {"1": "replaced"})
    dft = map_channels_to_values(df, {"b": {"1": "replaced"}})
    assert dft.b.iloc[0] == "replaced"

    dft = df.copy()
    dft.c = match_replace_types(dft.c, {"d": "replaced"})
    dft = map_channels_to_values(df, {"c": {"d": "replaced"}})
    assert dft.c.iloc[0] == "replaced"


def test_map_channels_to_values():
    # throw error
    data = pd.DataFrame(
        [
            {"a": 1, "b": 3, "Cd": 2},
            {"a": 2, "b": 3, "Cd": 5},
            {"a": 1, "b": 1, "Cd": 4}
        ]
    )

    try:
        _ = map_channels_to_values(
            data.copy(), {"aa": {1: "replaced", 2: "replaced", 3: "replaced"}}
        )
        assert False

    except InvalidInformationError:
        assert True

    #  best case
    mapped_data = map_channels_to_values(
        data.copy(), {"A": {1: "replaced", 2: "replaced", 3: "replaced"}}
    )

    replaced = data.copy()
    replaced["a"] = "replaced"
    assert (mapped_data == replaced).all().all()

    # wrong types
    mapped_data = map_channels_to_values(
        data.copy(), {"CD": {"2": "replaced", "5": "replaced", "4": "replaced"}}
    )

    replaced = data.copy()
    replaced["Cd"] = "replaced"
    assert (mapped_data == replaced).all().all()


def test_map_channels_to_values_empty():
    data = pd.DataFrame(
        [
            {"a": 1, "b": 3, "Cd": 2},
            {"a": 2, "b": 3, "Cd": 5},
            {"a": 1, "b": 1, "Cd": 4}
        ]
    )

    mapped_data = map_channels_to_values(
        data.copy(), {}
    )

    assert mapped_data.to_dict() == data.to_dict()


def test_reindex():
    data = pd.DataFrame(
        [
            {"a": 1, "b": 1},
            {"a": 1, "b": 2},
            {"a": 1, "b": 3},
            {"a": 1, "b": 4},
            {"a": 66, "b": 5},
            {"a": 1, "b": 6},
            {"a": 1, "b": 7},
            {"a": 1, "b": 8},
            {"a": 1, "b": 9},
            {"a": 1, "b": 10},
        ],
        index=pd.date_range(
            "20000101000100",
            "20000101001000",
            periods=10
        )

    )

    idx = pd.to_datetime("2000-01-01 00:04:00")
    del_data = data.drop(
        labels=[
            idx
        ],
        axis=0
    )
    assert reindex(del_data).loc[idx, :].isna().all()


def test_resample():
    data = pd.DataFrame(
        [
            {"a": 2, "b": 1},
            {"a": 2, "b": 2},
            {"a": 2, "b": 3},
            {"a": 2, "b": 4},
            {"a": 3, "b": 5},
            {"a": 3, "b": 6},
            {"a": 3, "b": 7},
            {"a": 3, "b": 8},
            {"a": 3, "b": 9},
            {"a": 4, "b": 10},
        ],
        index=pd.date_range(
            "20000101000100",
            "20000101001000",
            periods=10
        )
    )

    result = pd.DataFrame(
        [
            {"a": 2, "b": 2.5},
            {"a": 3, "b": 7},
            {"a": 4, "b": 10}
        ],
        index=[
            pd.to_datetime("2000-01-01 00:00:00"),
            pd.to_datetime("2000-01-01 00:05:00"),
            pd.to_datetime("2000-01-01 00:10:00"),
        ]
    )

    assert (result == resample(data, "5min")).all().all()


def test_plot_all_columns_groups(tmp_path, test_data):
    test_data.index = test_data["Datetime"]
    del test_data["Datetime"]

    identifier = "abc"

    plot_data(
        test_data,
        group_by_column="Valve_ai",
        exclude_columns="No.",
        accumulate="",
        save_path=tmp_path,
        color_map={
            '1': "#e9c46a"
        },
        identifier=identifier,
        parameter_unit_map={
            "Flow_rSig": "[l/min]",
            "O2_rSig": "[%]",
            "N2O_rSig": "[ppm]",
            "NO_rSig": "[ppm]",
            "CH4_rSig": "[ppm]"
        }
    )

    assert (tmp_path / f"{identifier}_N2O_rSig.html").exists()
    assert (tmp_path / f"{identifier}_NO_rSig.html").exists()
    assert (tmp_path / f"{identifier}_O2_rSig.html").exists()
    assert (tmp_path / f"{identifier}_CH4_rSig.html").exists()
    assert (tmp_path / f"{identifier}_Flow_rSig.html").exists()
