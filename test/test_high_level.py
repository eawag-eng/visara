#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: High level interface and functionalities.
# ==============================================================================
from os.path import join
from json import dumps
from visara.api.high_level import (
    init,
    reset,
    synchronize,
    plot,
    export
)

import os
from pathlib import Path


def test_reset(fake_config, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_reset!")

    init(fake_config, init_file)
    db_file = join(
        fake_config["PATHS"]["data_directory"],
        fake_config["DATABASE"]["database_file"]
    )
    assert os.path.exists(db_file)

    reset(fake_config)
    assert not os.path.exists(db_file)


def test_init(fake_config, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_init!")

    init(fake_config, init_file)
    db_file = join(
        fake_config["PATHS"]["data_directory"],
        fake_config["DATABASE"]["database_file"]
    )
    assert os.path.exists(db_file)


def test_synchronize(fake_config, upload_to_ftp, clean_up, ftpserver, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_synchronize!")

    init(fake_config, init_file)
    synchronize(fake_config)

    # 2 files and the database
    assert len(list(fake_config["PATHS"]["data_directory"].iterdir())) == 3
    reset(fake_config)
    clean_up(
        Path(ftpserver.get_local_base_path())
    )


def test_plot(fake_config, upload_to_ftp, clean_up, ftpserver, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_plot!")

    init(fake_config, init_file)
    synchronize(fake_config)
    plot(
        fake_config,
        section="PLOT_DEFAULT"
    )
    reset(fake_config)
    assert len(list(fake_config["PATHS"]["plot_directory"].iterdir())) == 6
    clean_up(
        Path(ftpserver.get_local_base_path())
    )


def test_plot_accumulate(fake_config, upload_to_ftp, clean_up, ftpserver, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_plot!")

    fake_config["PLOT_DEFAULT"]["accumulate"] = "1H"

    init(fake_config, init_file)
    synchronize(fake_config)
    plot(
        fake_config,
        section="PLOT_DEFAULT"
    )
    reset(fake_config)
    assert len(list(fake_config["PATHS"]["plot_directory"].iterdir())) == 6
    clean_up(
        Path(ftpserver.get_local_base_path())
    )


def test_plot_changed_settings(fake_config, upload_to_ftp, clean_up, ftpserver, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_plot_changed_settings!")

    fake_config["DATABASE"]["table_name"] = "table_name"
    fake_config["PLOT_DEFAULT"]["time_start"] = "-1hour"
    fake_config["PLOT_DEFAULT"]["identifier"] = "id"
    fake_config["PLOT_DEFAULT"]["parameter_to_unit"] = dict(
        Flow_rSig="[unit]",
        O2_rSig="[unit]",
        N2O_rSig="[unit]",
        NO_rSig="[unit]",
        CH4_rSig="[unit]"
    )
    fake_config["PLOT_DEFAULT"]["line_width"] = 3.2
    fake_config["PLOT_DEFAULT"]["marker_size"] = 3.2
    fake_config["PLOT_DEFAULT"]["exclude_columns"] = ["No.", "Valve_ai"]

    init(fake_config, init_file)
    synchronize(fake_config)
    plot(
        fake_config,
        section="PLOT_DEFAULT"
    )
    reset(fake_config)

    count = 0
    for file in fake_config["PATHS"]["plot_directory"].iterdir():
        count += 1
        assert "id" in file.name
    assert count == 5

    clean_up(
        Path(ftpserver.get_local_base_path())
    )


def test_export(fake_config, upload_to_ftp, clean_up, ftpserver, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_export!")

    init(fake_config, init_file)
    synchronize(fake_config)
    export(
        fake_config,
        section="EXPORT_ALL",
        with_mapped_channel_to_value=False
    )
    reset(fake_config)

    for file in fake_config["PATHS"]["export_directory"].iterdir():
        assert "Bio1_G1" not in file.read_text()
    assert file

    clean_up(
        Path(ftpserver.get_local_base_path())
    )


def test_export_changed_settings(fake_config, upload_to_ftp, clean_up, ftpserver, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_export_changed_settings!")

    fake_config["DATABASE"]["table_name"] = "table_name"
    fake_config["EXPORT_ALL"]["time_start"] = "-1hour"

    init(fake_config, init_file)
    synchronize(fake_config)
    export(
        fake_config,
        section="EXPORT_ALL",
        with_mapped_channel_to_value=False
    )
    reset(fake_config)

    file = ""
    for file in fake_config["PATHS"]["export_directory"].iterdir():
        # empty file only header
        assert len(file.read_text().split("\n")) == 2
    assert file

    clean_up(
        Path(ftpserver.get_local_base_path())
    )


def test_export_mapped(fake_config, upload_to_ftp, clean_up, ftpserver, init_file):

    from visara import LOGGER
    LOGGER.verbose_on()
    LOGGER.debug_on()

    LOGGER.info("\nRunning test_export_mapped!")

    init(fake_config, init_file)
    synchronize(fake_config)
    export(
        fake_config,
        section="EXPORT_ALL",
        with_mapped_channel_to_value=True
    )
    reset(fake_config)

    file = ""
    for file in fake_config["PATHS"]["export_directory"].iterdir():
        assert "Bio1_G1" in file.read_text()
    assert file

    clean_up(
        Path(ftpserver.get_local_base_path())
    )

