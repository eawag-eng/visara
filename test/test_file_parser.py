#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: Reading and Sorting files
# ==============================================================================
import pandas as pd

from pathlib import Path
from textwrap import dedent

from visara.core.errors import ConfigurationError
from visara.core.data_functionality import (
    parse_file,
    sort_files
)


def test_parse_file(shared_datadir):
    for file in shared_datadir.iterdir():
        data = parse_file(
            file,
            sep=","
        )

        assert isinstance(data, pd.DataFrame)
        assert data.shape == (50, 8)


def test_parse_irregular_file_1(tmp_path):

    test_file = tmp_path / "file.csv"

    test_file.write_text(
        dedent(
            """No.,DateTime,Valve_ai,N2O_rSig,NO_rSig,O2_rSig,CH4_rSig,Flow_rSig,
            1,26.06.2020 00:00:00,1.00,0.00,0.00,17.32,55.33,0.85,
            """
        )
    )

    data = parse_file(
        file=test_file,
        sep=",",
        date_col="",
        time_col="",
        datetime_col="DateTime"
    )
    assert sorted(data.columns.to_list()) == sorted([
        "No_", "Datetime", "Valve_ai", "N2O_rSig", "NO_rSig", "O2_rSig", "CH4_rSig", "Flow_rSig"
    ])


def test_parse_irregular_file_2(tmp_path):

    test_file = tmp_path / "file.csv"

    test_file.write_text(
        dedent(
            """No.,Date,Time,Valve_ai,N2O_rSig,NO_rSig,O2_rSig,CH4_rSig,,Flow_rSig,,,
            1,2020-06-06,00:00:00,1.00,0.00,0.00,17.32,55.33,,0.85,,,
            """
        )
    )

    data = parse_file(
        file=test_file,
        sep=",",
        date_col="Date",
        time_col="Time",
        datetime_col="",
        date_time_format="%Y-%m-%d %H:%M:%S"
    )
    assert sorted(data.columns.to_list()) == sorted([
        "No_", "Datetime", "Valve_ai", "N2O_rSig", "NO_rSig", "O2_rSig", "CH4_rSig", "Flow_rSig"
    ])


def test_parse_raise(tmp_path):

    test_file = tmp_path / "file.csv"

    test_file.write_text(
        dedent(
            """No.,Date,Time,Valve_ai,N2O_rSig,NO_rSig,O2_rSig,CH4_rSig,Flow_rSig,
            1,26.06.2020,00:00:00,1.00,0.00,0.00,17.32,55.33,0.85,
            """
        )
    )

    try:
        _ = parse_file(
            file=test_file,
            sep=",",
            date_col="Date",
            time_col="Time",
            datetime_col="DateTime"
        )
        assert False

    except ConfigurationError:
        assert True

    try:
        _ = parse_file(
            file=test_file,
            sep=",",
            date_col="",
            time_col="Time",
            datetime_col="DateTime"
        )
        assert False

    except ConfigurationError:
        assert True

    try:
        _ = parse_file(
            file=test_file,
            sep=",",
            date_col="Date",
            time_col="",
            datetime_col="DateTime"
        )
        assert False

    except ConfigurationError:
        assert True

    try:
        _ = parse_file(
            file=test_file,
            sep=",",
            date_col="Date",
            time_col="",
            datetime_col=""
        )
        assert False

    except ConfigurationError:
        assert True

    try:
        _ = parse_file(
            file=test_file,
            sep=",",
            date_col="",
            time_col="Time",
            datetime_col=""
        )
        assert False

    except ConfigurationError:
        assert True


def test_sort():
    files = [
        Path("logfile_03012020.csv"),
        Path("logfile_31012019.csv"),
        Path("logfile_03012019.csv")
    ]

    assert sort_files(
        files,
        "logfile_%d%m%Y.csv"
    ) == [
        Path("logfile_03012019.csv"),
        Path("logfile_31012019.csv"),
        Path("logfile_03012020.csv")
    ]


def test_sort_other_format():
    files = [
        Path("uhu_03012020.csv"),
        Path("uhu_31012019.csv"),
        Path("uhu_03012019.csv")
    ]

    assert sort_files(
        files,
        "uhu_%d%m%Y.csv"
    ) == [
        Path("uhu_03012019.csv"),
        Path("uhu_31012019.csv"),
        Path("uhu_03012020.csv")
    ]

