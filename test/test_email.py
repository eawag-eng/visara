from visara import log_error

from os import listdir


def test_email(fake_config, email):

    fake_config["NOTIFICATIONS"]["email"] = email

    try:
        raise Exception("This is a test error!")

    except Exception as e:
        log_error(
            fake_config,
            e,
            subject="VISARA TEST EMAIL",
            message="THIS IS A TEST ERROR MESSAGE! YOU RECEIVED IT, THAT MEANS THE TEST HAS PASSED!"
        )
    assert len(listdir(fake_config["PATHS"]["logging_directory"])) == 1
