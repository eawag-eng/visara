#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: Several datetime conversions.
# ==============================================================================
from datetime import (
    datetime,
    timedelta
)

from visara.core.datetime_parser import (
    format_output,
    parse_datetime
)


def test_format_output():
    assert format_output(
        datetime(2020, 1, 1, 1, 1, 1)
    ) == "2020-01-01 01:01:01"

    assert format_output(
        "2020-01-01 01:01:01"
    ) == "2020-01-01 01:01:01"

    assert format_output(
        "2020-01-01 01:01"
    ) == "2020-01-01 01:01:00"

    assert format_output(
        "2020-01-01 01"
    ) == "2020-01-01 01:00:00"

    assert format_output(
        "2020-01-01"
    ) == "2020-01-01 00:00:00"

    try:
        format_output(
            "2020-01"
        )
        success = True
    except:
        success = False
        pass

    assert not success


def test_parse_datetime():
    assert parse_datetime(
        "now"
    ) == datetime.now().strftime("%F %X")

    assert parse_datetime(
        "-2day"
    ) == (
        datetime.now() - timedelta(days=2)
    ).strftime("%F %X")

    assert parse_datetime(
        "-2      day"
    ) == (
        datetime.now() - timedelta(days=2)
    ).strftime("%F %X")

    assert parse_datetime(
        "  2      hour  "
    ) == (
       datetime.now() - timedelta(hours=2)
    ).strftime("%F %X")
