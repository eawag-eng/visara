from visara import LOGGER, log_error

from os import listdir


def test_verbose():
    LOGGER.debug_on(False)
    LOGGER.verbose_on()
    LOGGER.info("YOU SHOULD SEE THIS")
    LOGGER.debug("YOU SHOULD NOT SEE THIS")
    LOGGER.verbose_on(False)
    LOGGER.info("YOU SHOULD NOT SEE THIS")
    LOGGER.debug("YOU SHOULD NOT SEE THIS")


def test_debug():
    LOGGER.verbose_on(False)
    LOGGER.debug_on()
    LOGGER.info("YOU SHOULD NOT SEE THIS")
    LOGGER.debug("YOU SHOULD SEE THIS")
    LOGGER.debug_on(False)
    LOGGER.info("YOU SHOULD NOT SEE THIS")
    LOGGER.debug("YOU SHOULD NOT SEE THIS")


def test_error(fake_config):

    try:
        raise Exception("This is a test error!")

    except Exception as e:
        log_error(
            fake_config,
            e
        )

    assert len(listdir(fake_config["PATHS"]["logging_directory"])) == 1
