#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: List memory utilities.
# ==============================================================================
from visara.core.memory import write_memory, delete_memory, read_memory

from pathlib import Path


def test_write_memory(tmp_path):
    write_memory(tmp_path, "test", ["a", "b", "c"])
    assert Path(tmp_path / "test.memory").exists()


def test_read_memory_init(tmp_path):
    content = read_memory(tmp_path, "test")
    assert content == []


def test_read_memory_filled(tmp_path):
    write_memory(tmp_path, "test", ["a", "b", "c"])
    content = read_memory(tmp_path, "test")
    assert content == ["a", "b", "c"]


def test_read_memory_multiple_fills(tmp_path):
    write_memory(tmp_path, "test", ["a", "b", "c"])
    content_1 = read_memory(tmp_path, "test")
    write_memory(tmp_path, "test", ["e", "f", "g"])
    content_2 = read_memory(tmp_path, "test")
    write_memory(tmp_path, "test", ["a", "b", "c"])
    content_3 = read_memory(tmp_path, "test")

    assert content_1 == ["a", "b", "c"]
    assert content_2 == ["a", "b", "c", "e", "f", "g"]
    assert content_3 == ["a", "b", "c", "e", "f", "g", "a", "b", "c"]


def test_delete_memory_init(tmp_path):

    write_memory(tmp_path, "test", ["a"])
    assert (tmp_path / "test.memory").exists()

    delete_memory(tmp_path, "test")
    assert not ((tmp_path / "test.memory").exists())
