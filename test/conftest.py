#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: All pytest fixtures are here.
# ==============================================================================
import pytest

from textwrap import dedent
from pathlib import Path

import pandas as pd

from visara.core.ftp import FtpUtil
from visara.core.generate import generate_conf
from visara.core.config import read_config


def pytest_addoption(parser):
    parser.addoption(
        "--run-email-test", action="store", default="", help="please enter a valid email_address"
    )


@pytest.fixture
def email(request):
    return request.config.getoption("--run-email-test")


def pytest_collection_modifyitems(config, items):
    if not config.getoption("--run-email-test"):
        return
    skip_annoying = pytest.mark.skip(
        reason="need --run-email-tests with valid email option to run eg: --run-email-tests christian.foerster@eawag.ch"
    )
    for item in items:
        if item.parent.name.startswith("test_email"):
            item.add_marker(skip_annoying)


@pytest.fixture()
def fake_config(tmp_path, ftpserver):
    ini = tmp_path / "config.ini"
    generate_conf(ini)
    config = read_config(ini)

    config["FTP"] = dict(
        host="localhost",
        port=ftpserver.server_port,
        usr=ftpserver.username,
        pwd=ftpserver.password
    )

    config["PATHS"] = {
        "data_directory": tmp_path / "data",
        "plot_directory": tmp_path / "plot",
        "export_directory": tmp_path / "export",
        "logging_directory": tmp_path / "logs"

    }

    config["PLOT_DEFAULT"].update(
        dict(
            time_start="1970-01-01",
            time_end="now",
            accumulate="20s"
        )
    )
    return config


@pytest.fixture()
def local_folder(tmp_path):
    lf = tmp_path / "local"
    lf.mkdir()
    return lf


@pytest.fixture()
def remote_folder(tmp_path):
    rf = tmp_path / "remote"
    rf.mkdir()
    return rf


@pytest.fixture()
def generate_files():
    def _generate_files(folder, start, end):
        files = []
        for f in range(start, end):
            fname = f"{f}.csv"
            file = folder / fname
            file.write_text(
                f"{f}" * 100
            )
            files.append(fname)

        return files
    return _generate_files


@pytest.fixture()
def clean_up():
    def _clean_up(folder):
        for file in folder.iterdir():
            if not file.name.startswith("."):
                file.unlink()
        return
    return _clean_up


@pytest.fixture()
def get_ftp_tools():
    def _get_ftp_tools(ftp_server):
        _ftp = FtpUtil(
            host="localhost",
            port=ftp_server.server_port,
            usr=ftp_server.username,
            pwd=ftp_server.password,
        )

        return Path(ftp_server.get_local_base_path()), _ftp
    return _get_ftp_tools


@pytest.fixture()
def init_file(shared_datadir):
    for file in shared_datadir.iterdir():
        return shared_datadir / file


@pytest.fixture()
def upload_to_ftp(ftpserver, shared_datadir):
    _ftp = FtpUtil(
        host="localhost",
        port=ftpserver.server_port,
        usr=ftpserver.username,
        pwd=ftpserver.password,
        )

    for file in shared_datadir.iterdir():
        _ftp.upload(
            shared_datadir / file,
            "/"
        )


@pytest.fixture()
def database(tmp_path):
    return tmp_path / "database.sqlite"


@pytest.fixture()
def table_name():
    return "visara"


@pytest.fixture()
def creation_query(table_name):
    return dedent(
        f"""
        CREATE TABLE IF NOT EXISTS {table_name} ( 
            Datetime datetime UNIQUE, 
            No_ integer, 
            Valve_ai integer, 
            N2O_rSig real, 
            NO_rSig real, 
            CH4_rSig real, 
            O2_rSig real, 
            Flow_rSig real
        );
        """
    )


@pytest.fixture()
def test_data():
    data = pd.DataFrame(
        [
            {
                'No_': '1',
                'Valve_ai': '1.0',
                'N2O_rSig': '0.0',
                'NO_rSig': '0.0',
                'O2_rSig': '20.18',
                'CH4_rSig': '19.76',
                'Flow_rSig': '0.85',
                'Datetime': '2020-06-27 00:00:00'
            }, {
                'No_': '2',
                'Valve_ai': '1.0',
                'N2O_rSig': '0.0',
                'NO_rSig': '0.0',
                'O2_rSig': '20.18',
                'CH4_rSig': '19.90',
                'Flow_rSig': '0.85',
                'Datetime': '2020-06-27 00:00:01'
            }, {
                'No_': '3',
                'Valve_ai': '1.0',
                'N2O_rSig': '0.0',
                'NO_rSig': '0.0',
                'O2_rSig': '20.19',
                'CH4_rSig': '19.99',
                'Flow_rSig': '0.85',
                'Datetime': '2020-06-27 00:00:02'
            }, {
                'No_': '4',
                'Valve_ai': '1.0',
                'N2O_rSig': '0.0',
                'NO_rSig': '0.0',
                'O2_rSig': '20.19',
                'CH4_rSig': '20.06',
                'Flow_rSig': '0.85',
                'Datetime': '2020-06-27 00:00:03'
            }
        ]
    )

    for col in data.columns:
        if col == "Datetime":
            data[col] = pd.to_datetime(data[col])
        else:
            data[col] = pd.to_numeric(data[col])

    return data



