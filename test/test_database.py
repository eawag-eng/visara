#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: Basic database functionalities
# ==============================================================================
from visara.core.db_tools import DataBase
from visara.core.data_functionality import (
    init_database_and_table_if_not_exists,
    delete_database,
    update_database,
    fill_database,
    get_last_row_from_database,
    read_database,
    match_pandas_to_sqlite_types,
    generate_table_query_from_col_type_map,
    generate_table_creation_query
)

import pandas.api.types


def test_match_pandas_to_sqlite_types(test_data):

    test_data["string"] = "abcd"

    assert match_pandas_to_sqlite_types(
       test_data
    ) == {
        'No_': 'integer',
        'Valve_ai': 'real',
        'N2O_rSig': 'real',
        'NO_rSig': 'real',
        'O2_rSig': 'real',
        'CH4_rSig': 'real',
        'Flow_rSig': 'real',
        'Datetime': 'datetime',
        'string': 'text'
    }


def test_generate_table_query_from_col_type_map(tmp_path, test_data):
    type_map = match_pandas_to_sqlite_types(
        test_data
    )
    creation_query = generate_table_query_from_col_type_map(type_map)

    init_database_and_table_if_not_exists(
        tmp_path / "database.sqlite",
        creation_query
    )


def test_generate_table_creation_query(tmp_path, test_data):

    creation_query = generate_table_creation_query(
        test_data
    )

    init_database_and_table_if_not_exists(
        tmp_path / "database.sqlite",
        creation_query
    )


def test_init(database, table_name, creation_query):

    init_database_and_table_if_not_exists(database, creation_query)
    assert database.exists()

    db = DataBase(database)
    answer = db.query(
        f"SELECT name FROM sqlite_master WHERE type = 'table' AND name = '{table_name}';"
    )
    assert answer == [(table_name,)]


def test_delete(database):

    database.touch()
    assert database.exists()
    delete_database(database)
    assert not database.exists()


def test_fill_database(database, table_name, creation_query, test_data):
    init_database_and_table_if_not_exists(
        database,
        creation_query
    )

    cut = test_data.iloc[:-1, :]

    written_data = fill_database(
        cut,
        database,
        table_name
    )

    assert written_data == cut.to_dict(orient="records")

    last_row = test_data.iloc[-1:, :]

    written_data = fill_database(
        last_row,
        database,
        table_name
    )

    assert written_data == last_row.to_dict(orient="records")

    written_data = fill_database(
        test_data,
        database,
        table_name
    )
    assert written_data == []


def test_read_last_row(database, table_name, creation_query, test_data):
    init_database_and_table_if_not_exists(database, creation_query)
    row = get_last_row_from_database(database, table_name)
    assert row == []

    _ = fill_database(
        test_data,
        database,
        table_name
    )
    row = get_last_row_from_database(database, table_name)
    assert row == [('2020-06-27 00:00:03',)]


def test_update_database(database, table_name, creation_query, shared_datadir):
    init_database_and_table_if_not_exists(
        database,
        creation_query
    )
    filled = update_database(
        shared_datadir,
        database,
        return_filled=True,
        table_name=table_name
    )

    assert len(filled) == 100

    filled = update_database(
        shared_datadir,
        database,
        return_filled=True,
        table_name=table_name
    )

    assert len(filled) == 0


def test_read_database(database, table_name, creation_query, test_data):
    init_database_and_table_if_not_exists(database, creation_query)

    _ = fill_database(
        test_data,
        database,
        table_name
    )

    time_start = "2020-06-27 00:00:01"
    time_end = "2020-06-27 00:00:03"

    data = read_database(
        database,
        time_start,
        time_end,
        table_name
    )

    assert data.shape[0] == 3

    time_start = "2020-06-27 00:00:01"
    time_end = "2020-06-27 00:00:02"

    data = read_database(
        database,
        time_start,
        time_end,
        table_name
    )

    assert data.shape[0] == 2

    for col in data.columns:
        assert not pandas.api.types.is_string_dtype(data[col])
