#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright 2021 Christian Foerster @EAWAG. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Testing: Config parser functionality.
# ==============================================================================
from visara.core.config import read_config
from visara.core.generate import generate_conf

from textwrap import dedent


def test_read_config(tmp_path):

    ini = tmp_path / "config.ini"

    generate_conf(ini)

    assert ini.exists()
    assert read_config(ini) != {}


def test_read_config_conversions(tmp_path):
    ini = tmp_path / "config.ini"

    ini.write_text(
        dedent(
            """
            [DEFAULT]
            a = test
            b = 1
            c = 2.3
            d = {"a":3.2, "b":3}
            e = [33,[],"dd",2.3]
            
            [ANOTHER SECTION]
            f = 3
            
            [INDENTED_1]
            a = 
                b=2
                c =2.3
                d = abc
                e={3:3}
                f= [3,4,5,[]] 
            
            [INDENTED_2]
            a = b=2
                c =2.3
            """
        )
    )

    conf = read_config(ini)

    assert isinstance(conf["DEFAULT"]["a"], str)
    assert isinstance(conf["DEFAULT"]["b"], int)
    assert isinstance(conf["DEFAULT"]["c"], float)
    assert isinstance(conf["DEFAULT"]["d"], dict)
    assert isinstance(conf["DEFAULT"]["d"]["a"], float)
    assert isinstance(conf["DEFAULT"]["d"]["b"], int)
    assert isinstance(conf["DEFAULT"]["e"], list)
    assert isinstance(conf["DEFAULT"]["e"][0], int)
    assert isinstance(conf["DEFAULT"]["e"][1], list)
    assert isinstance(conf["DEFAULT"]["e"][2], str)
    assert isinstance(conf["DEFAULT"]["e"][3], float)
    assert isinstance(conf["ANOTHER SECTION"]["f"], int)
    assert isinstance(conf["INDENTED_1"]["a"], dict)
    assert isinstance(conf["INDENTED_1"]["a"]["b"], int)
    assert isinstance(conf["INDENTED_1"]["a"]["c"], float)
    assert isinstance(conf["INDENTED_1"]["a"]["d"], str)
    assert isinstance(conf["INDENTED_1"]["a"]["e"], dict)
    assert isinstance(conf["INDENTED_1"]["a"]["f"], list)
    assert conf["INDENTED_1"]["a"]["f"][3] == []
    assert isinstance(conf["INDENTED_2"]["a"], dict)
    assert isinstance(conf["INDENTED_1"]["a"]["b"], int)
    assert isinstance(conf["INDENTED_1"]["a"]["c"], float)



